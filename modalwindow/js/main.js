"use strict";

{
  const $open = document.getElementById("open");
  const $mask = document.getElementById("mask");
  const $modalContent = document.getElementById("modalContent");
  const $close = document.getElementById("close");

  $open.addEventListener("click", () => openClick());
  $mask.addEventListener("click", () => closeClick());
  $close.addEventListener("click", () => closeClick());
  const openClick = () => {
    console.log($mask);
    $mask.classList.remove("hidden");
    $modalContent.classList.remove("hidden");
  };
  const closeClick = () => {
    console.log($mask);
    $mask.classList.add("hidden");
    $modalContent.classList.add("hidden");
  };
}
