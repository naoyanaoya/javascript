"use strict";

//stackoverflow.com/questions/3822248/using-jquery-getjson-with-other-javascripts-included-gives-referenceerror
// https: $.getJSON("article.json", function (data) {
$.getJSON("./article.json", function (data) {
  // console.log("It worked!");
  console.log(data);
  console.log(data[0]);
  // for (let index = 0; index < data.length; index++) {
  //   console.log(`${data[index].id} ${data[index].name}`);
  // }
  // console.log(data[0].id);
  // console.log(data[0].name);
  // console.log("--------------");
  // console.log(Object.keys(data)[0]);
  // console.log(Object.keys(data).length);
});
// or
$.ajax({
  url: "articles.json",
  dataType: "json",
  type: "get",
  cache: false,
  success: function (data) {
    // console.log(data);
    // console.log(data.articles);
    // $(data.articles).each(fucntion(index, value) {
    //   console.log("It Worked!");
    // },
    // );
    $(data.articles).each(function (index, value) {
      // console.log(value);
      // console.log(`${index} id is ${value.id}, and ${value.name}`);
      const addContents = `<li>${index} id is ${value.id}, and ${value.name}.</li>`;
      $("#list").append(addContents);
    });
  },
});

// $("#button1").click(() => {
//   $("#div1").hide();
// });
// $("#button2").click(() => {
//   $("#div1").fadeOut();
// });
// $("#button3").click(() => {
//   $("#div1").show();
// });
// $("#button4").click(() => {
//   $("#div1").fadeIn();
// });
// $("#button5").click(() => {
//   $("#div1").css("background-color", "Red");
// });

// const to_json = [
//   {
//     Name: "JsonData1",
//     Desc: "JavaScript Object Notation1",
//     UseInt: 1,
//     UseString: "String1",
//     UseArray: [11, 12, 13, 14, 15],
//   },

//   {
//     Name: "JsonData2",
//     Desc: "JavaScript Object Notation2",
//     UseInt: 2,
//     UseString: "String2",
//     UseArray: [21, 22, 23, 24, 25],
//   },

//   {
//     Name: "JsonData3",
//     Desc: "JavaScript Object Notation3",
//     UseInt: 3,
//     UseString: "String3",
//     UseArray: [31, 32, 33, 34, 35],
//   },
// ];

// https://www.pasonatech.co.jp/workstyle/column/detail.html?p=2410
// const json_string = JSON.stringify(to_json);
// console.log(json_string[0].Name); // 参照できない
// const from_json = JSON.parse(json_string);
// console.log(from_json);
// console.log(from_json[0].Name);

// const students = [
//   {
//     name: "yamada",
//     school_year: 2,
//     test: {
//       japanese: 65,
//       english: 80,
//     },
//   },
//   {
//     name: "hayashi",
//     school_year: 2,
//     test: {
//       japanese: 45,
//       english: 95,
//     },
//   },
//   {
//     name: "sato",
//     school_year: 1,
//     test: {
//       japanese: 75,
//       english: 35,
//     },
//   },
// ];
// const students_from_json = JSON.parse(students);
// console.log(students_from_json);
// console.log(students);
// console.log(Object.keys(students).length);
// console.log(students[0].name);
// console.log(students[0].school_year);
// console.log(students[0].test.japanese);
// console.log(students[0].test.english);
// console.log(Object.keys(students[0].test).length);
// const students_json_obj = JSON.parse(students[0]);
// console.log(students_json_obj.count);
// JSON.parse(students);
