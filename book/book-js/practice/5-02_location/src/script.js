// "use strict";

// querySelectorのcssセレクタ
const lang = document.querySelector("html").lang;

if (lang === "ja") {
  // <option>タグのうち、value属性が"index.html"であるものにマッチする
  document.querySelector('option[value="index.html"]').selected = true;
} else if (lang === "en") {
  document.querySelector('option[value="index-en.html"]').selected = true;
} else if (lang === "zh") {
  document.querySelector('option[value="index-zh.html"]').selected = true;
}

// イベントの設定
// onchangeイベントは、フォームに入力された内容が変わったときに発生する
// テキストフィールなら入力内容が変わったとき、
// プルダウンメニューであれば選択項目が切り替わったときに発生する
document.getElementById("form").select.onchange = function (e) {
  // URLを書き換える
  e.preventDefault();
  // console.log(document.getElementById("form").select.value);
  location.href = document.getElementById("form").select.value;
  // location.href =
  //   "https://www.youtube.com/watch?v=dmtW25vLI5E&ab_channel=%E7%80%AC%E6%88%B8%E5%BC%98%E5%8F%B8%2FKojiSeto";
};
