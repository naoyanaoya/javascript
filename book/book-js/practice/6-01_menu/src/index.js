// h3がクリックされたら、弟要素である<ul>の表示・非表示を切り替える
//
// HTMLが読み込まれたら、functionの{~}の処理をする
// $(document).ready(function() {
// });
$(document).ready(function () {
  // <div class='submenu'>の中にある<h3>を取得する
  // $('.submenu h3')
  $(".submenu h3").on("click", function () {
    $(this).next().toggleClass("hidden");
  });
});
