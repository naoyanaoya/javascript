// document.getElementById("form").onsubmit = function (event) {
//   console.log("クリックされました");
// };

//   送信ボタンがクリックされたタイミングで
//       name属性を使ってテキストフィールドの入力内容を読み取り
//       <p id='output' ></p>
//       に出力されるプログラム

document.getElementById("form").onsubmit = function (event) {
  // <form>要素の取得 document.getElementById('form')
  // 入力された内容を読み取りたいフォーム部品を、name属性で指定する
  // document.getElementById('form').word
  // document.getElementById('form').word.value

  // documentオブジェクトには、ブラウザに表示されているHTMLや、
  // それに関連すうるCSSを操作するための昨日が多数用意されている
  // getElementByIdメソッドは、()内に指定されたid名を持つ要素をまるごと取得する
  const search = document.getElementById("form").word.value;
  console.log(search);
  document.getElementById("output").textContent = `「${search}」の検索中...`;

  //   ページの再読み込みをしないようにするためのコード
  event.preventDefault();
};
