let image_list = [
  { key1: "./images/thumb-img1.jpg", key2: "./images/img1.jpg" },
  { key1: "./images/thumb-img2.jpg", key2: "./images/img2.jpg" },
  { key1: "./images/thumb-img3.jpg", key2: "./images/img3.jpg" },
  { key1: "./images/thumb-img4.jpg", key2: "./images/img4.jpg" },
];

for (let item of image_list) {
  const li = `<li>
  <img src="${item.key1}" class="thumb" data-image="${item.key2}" />
</li>`;
  // console.log(li);
  document.getElementById("list").insertAdjacentHTML("beforeend", li);
}

// 要素を取得する→イベントを設定する→イベント発生時の処理を作る
// document.querySelectorAll('CSSセレクタ')
// .classname
// で指定されたclass属性を持つすべての要素を選択する
// セレクタに「.thumb」を指定しているので、<img class="thumb"...>がすべて取得される
const thumbs = document.querySelectorAll(".thumb");
// console.log(thumbs);
thumbs.forEach(function (item, index) {
  // console.log(item);
  // console.log(index);
  // パラメータitemに保存されている要素にonclickイベントを設定している
  item.onclick = function () {
    // thisは、イベントが発生したした要素、つまりクリックされた要素
    // thisは、イベントに設定するファンクションの中で使える
    // JavaScriptでdata-ナンデモ属性の値を読み取る
    // 取得した要素.dataset.ナンデモのところにつけた名前
    // console.log(this);
    // console.log(this.dataset);
    // console.log(this.dataset.image);
    document.getElementById("bigimg").src = this.dataset.image;
  };
});
