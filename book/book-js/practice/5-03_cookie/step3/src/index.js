const agree = Cookies.get("cookie-agree");
// 定数panelには<div id="privacy-panel">~</div>が代入されている
const panel = document.getElementById("privacy-panel");

if (agree === "yes") {
  // console.log("クッキーを確認しました");
  // HTML要素を削除する
  // removeChildメソッドを使用するには<div>の親要素である<body>要素を取得する
  // なので
  // document.body
  // とする
  document.body.removeChild(panel);
} else {
  // console.log("クッキーを確認できませんでした");
  document.getElementById("agreebtn").onclick = function () {
    Cookies.set("cookie-agree", "yes", { expires: 7 });
    document.body.removeChild(panel);
  };
}

// クッキー削除
document.getElementById("testbtn").onclick = function () {
  Cookies.remove("cookie-agree");
};
