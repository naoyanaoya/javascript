const images = [
  "./images/image1.jpg",
  "./images/image2.jpg",
  "./images/image3.jpg",
  "./images/image4.jpg",
  "./images/image5.jpg",
];
let current = 0;

function changeImage(num) {
  if (current + num >= 0 && current + num < images.length) {
    current += num;
    document.getElementById("main_image").src = images[current];
    // ページが変わったときに起きるfunction
    pageNum();
  }
}

function pageNum() {
  document.getElementById("page").textContent = `${current + 1}/${
    images.length
  }`;
}

// ページが読み込まれたときに、起こすfunction
// 最初にがページを開いたときにも起こるfunction
pageNum();

document.getElementById("prev").onclick = function () {
  changeImage(-1);
};
document.getElementById("next").onclick = function () {
  changeImage(1);
};
