"use strict";

const customerName = document.getElementById("customerName");
const password = document.getElementById("password");
const form = document.getElementById("form");
const formTest = document.getElementById("formTest");
const errorElement = document.getElementById("error");

// formTest.onsubmit = function () {
//   console.log("on click");
// };

document.getElementById("btn").addEventListener("click", () => {
  document.getElementById("textChange").innerHTML = "<b>クリック後</b>";
});

form.addEventListener("submit", (event) => {
  let messages = [];
  if (customerName.value === "" || customerName.value == null) {
    messages.push("Name is required");
  }
  if (password.value.length <= 6) {
    messages.push("Password must be longer than 6 characters.");
  }
  if (password.value.length >= 20) {
    messages.push("Password must be less than 20 characters.");
  }

  if (password.value === "password") {
    messages.push("Password cannot be password");
  }

  if (messages.length > 0) {
    event.preventDefault();
    errorElement.innerText = messages.join(", ");
    console.log(errorElement.innerText);
  }
});
