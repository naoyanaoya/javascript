const quiz = [
  {
    question: "1は英語で？",
    answers: ["one", "two", "three", "four"],
    correct: "one",
  },
  {
    question: "2は英語で？",
    answers: ["one", "two", "three", "four"],
    correct: "two",
  },
  {
    question: "3は英語で？",
    answers: ["one", "two", "three", "four"],
    correct: "three",
  },
  {
    question: "4は英語で？",
    answers: ["one", "two", "three", "four"],
    correct: "four",
  },
];

const quizLength = quiz.length;
let quizIndex = 0;
let score = 0;
const $button = document.getElementsByTagName("button");
const buttonLength = $button.length;

const setupQuiz = () => {
  document.getElementById("js-question").textContent = quiz[quizIndex].question;
  let buttonIndex = 0;
  while (buttonIndex < buttonLength) {
    $button[buttonIndex].textContent = quiz[quizIndex].answers[buttonIndex];
    buttonIndex++;
  }
};

setupQuiz();

const clickHandler = (e) => {
  if (quiz[quizIndex].correct === e.target.textContent) {
    window.alert("correct");
    score++;
  } else {
    window.alert("wrong");
  }

  // 次の問題に進む処理
  quizIndex++;
  if (quizIndex < quizLength) {
    setupQuiz();
  } else {
    window.alert("finish your score is " + score + "/" + quizLength + ".");
  }
};

let handlerIndex = 0;
while (handlerIndex < buttonLength) {
  $button[handlerIndex].addEventListener("click", (e) => {
    clickHandler(e);
  });
  handlerIndex++;
}
