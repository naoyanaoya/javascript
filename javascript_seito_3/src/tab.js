(() => {
  const $doc = document;
  const $tab = $doc.getElementById("js-tab");
  const $nav = $tab.querySelectorAll("[data-nav]");
  const $content = $tab.querySelectorAll("[data-content]");
  const ACTIVE_CLASS = "is-active";
  const navLen = $nav.length;
  // console.log("$content", $content);
  const init = () => {
    $content[0].style.display = "block";
  };
  init();

  // クリックされた際の挙動
  // =============================================================
  const handleClick = (e) => {
    // もともと指定されているlinkに飛ぶイベントを起こさないようにする
    e.preventDefault();

    // クリックされたnavとそのdataを取得
    const $this = e.target;
    const targetValue = $this.dataset.nav;

    // 対象外のnav,contentすべて一旦リセットする
    let index = 0;
    while (index < navLen) {
      $content[index].style.display = "none";
      $nav[index].classList.remove(ACTIVE_CLASS);
      index++;
    }

    // これでメモが見れる
    console.log($tab.querySelectorAll('[data-content="' + targetValue + '"]'));
    console.log(
      $tab.querySelectorAll('[data-content="' + targetValue + '"]')[0]
    );
    console.log(
      $tab.querySelectorAll('[data-content="' + targetValue + '"]')[0].style
        .display
    );

    // 対象のコンテンツをアクティブ化
    $tab.querySelectorAll(
      '[data-content="' + targetValue + '"]'
    )[0].style.display = "block";
    $nav[targetValue].classList.add(ACTIVE_CLASS);
  };
  // =============================================================

  let index = 0;
  while (index < navLen) {
    $nav[index].addEventListener("click", (e) => handleClick(e));
    index++;
  }
})();
